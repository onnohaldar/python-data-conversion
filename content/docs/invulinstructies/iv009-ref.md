---
title: IV009-ref
description: 'IV009-ref: Hoe moet worden omgegaan met een geboortedatum?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Wanneer de Geboortedatum niet volledig of onbekend is, dan wordt het deel dat wel bekend is gebruikt en wordt voor de overige delen de waarde 01 (dag en maand) of 1900 (jaar) gebruikt. Het element DatumGebruik geeft aan welk deel van de datum bekend is en dus te gebruiken. 

Voorbeelden: 
* Een volledig onbekende geboortedatum wordt 01-01-1900 
* Is alleen bekend dat de geboorte in 1953 was, dan wordt de geboortedatum 01-01-1953 
* Is alleen bekend dat de geboorte in september 1949 was, dan wordt de geboortedatum 01-09-1949
