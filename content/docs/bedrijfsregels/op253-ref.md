---
title: OP253-ref
description: 'OP253-ref: Indien het verwijderen of wijzigen van een melding van de start, mutatie of beeindiging van een zorglevering noodzakelijk is, moet dit zo spoedig mogelijk gemeld worden.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
