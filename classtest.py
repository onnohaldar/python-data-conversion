class IstdRegel:
    
    def __init__(self):
        self.code: str = None
        self.name: str = None
        self.documentatie = None
        self.wordtGebruiktIn = None

regels: IstdRegel = []
regels.append(IstdRegel())
regels[0].code = 'testcode0'
regels.append(IstdRegel())
regels[1].code = 'testcode1'

for regel in regels:
    print(regel.code)