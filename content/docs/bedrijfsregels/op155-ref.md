---
title: OP155-ref
description: 'OP155-ref: Een melding over een (tijdelijke) beeindiging van een zorglevering  moet een verwijzing bevatten naar de laatste melding van de start van diezelfde levering.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
