---
title: TR019-ref
description: 'TR019-ref: Een melding waarin wordt aangegeven dat een zorglevering gestart, beëindigd of gewijzigd wordt, moet verwijzen naar de toewijzing van de betreffende levering.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
