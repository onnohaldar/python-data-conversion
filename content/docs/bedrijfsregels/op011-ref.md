---
title: OP011-ref
description: 'OP011-ref: Begindatum van geleverde zorg of ondersteuning wordt gemeld nadat de levering gestart is.'
wordtGebruiktIn: 'iWmo 3.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
