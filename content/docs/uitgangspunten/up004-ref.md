---
title: UP004-ref
description: 'UP004-ref: De inzet van zorg of ondersteuning voor een client wordt gecoordineerd door de instantie die hier wettelijk gezien voor verantwoordelijk is.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Voor de Wlz is dit het regionale zorgkantoor, voor de Wmo en de Jeugdwet is dit de gemeente.
