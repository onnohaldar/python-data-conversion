---
title: UP003-ref
description: 'UP003-ref: De privacy van de client is in de iStandaarden geborgd doordat aangesloten ketenpartijen zich conformeren aan de Algemene Verordening Gegevensbescherming (AVG).'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Bij twijfel dient de (zorg)professional contact op te nemen met de functionaris gegevensbescherming (FG) of met de adviserende (branche)organisatie op het gebied van de AVG. 
