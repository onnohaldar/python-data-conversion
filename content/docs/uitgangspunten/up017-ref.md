---
title: UP017-ref
description: 'UP017-ref: Informatieuitwisseling volgens de iStandaarden is gebaseerd op gestandaardiseerd berichtenverkeer.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
