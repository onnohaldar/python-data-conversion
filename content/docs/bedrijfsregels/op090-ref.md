---
title: OP090-ref
description: 'OP090-ref: Voor ieder ontvangen bericht moet binnen een afgesproken periode na ontvangst een retourbericht verzonden worden.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Per iStandaard worden afspraken vastgelegd over de reactietermijn waarbinnen een ontvanger een retourbericht moet sturen aan de verzender.
