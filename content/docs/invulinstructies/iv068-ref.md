---
title: IV068-ref
description: 'IV068-ref: Hoe moeten percentages gevuld worden?'
wordtGebruiktIn: 'iWmo 3.0, iWlz 2.3, iJw 3.0, iWlz 2.2'
---
Percentages worden gevuld in honderdste procenten. Bijvoorbeeld: 9000 is negenduizend honderdste procent, is 90%.
