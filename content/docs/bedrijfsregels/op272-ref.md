---
title: OP272-ref
description: 'OP272-ref: De aanbieder verzendt een stopbericht binnen vijf werkdagen na de daadwerkelijke datum waarop de ondersteuning beeindigd is.'
wordtGebruiktIn: 'iWmo 3.1, iJw 3.1'
---
