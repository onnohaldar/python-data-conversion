---
title: TR101-ref
description: 'TR101-ref: Binnen een bericht zijn dubbele regels niet toegestaan.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Dat betekent dat voor iedere berichtklasse de logische sleutel in combinatie met de logische sleutels van de bovenliggende berichtklassen uniek moet zijn.
