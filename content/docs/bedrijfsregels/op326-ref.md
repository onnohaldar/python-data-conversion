---
title: OP326-ref
description: 'OP326-ref: Een gewijzigd bericht kan verwijderd worden.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
Een bericht dat eerder met status aanlevering '2' (Gewijzigde aanlevering) gewijzigd is, kan verwijderd worden met status aanlevering '3' (Verwijderen aanlevering). Resultaat van deze verwijdering is dat de aanlevering in zijn geheel (zowel de oorspronkelijke als de gewijzigde aanlevering(en)) komt te vervallen.
