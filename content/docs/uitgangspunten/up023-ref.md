---
title: UP023-ref
description: 'UP023-ref: Informatie wordt eenmalig bij de client uitgevraagd.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
