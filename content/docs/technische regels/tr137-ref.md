---
title: TR137-ref
description: 'TR137-ref: Einddatum vullen met een waarde die groter is dan, of gelijk is aan de Begindatum van de aangeduide periode en die niet groter is dan de Dagtekening van het bericht.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iWlz 2.2'
---
(NB: deze regel had regelcode CS108 in de releases iWlz 2.2, iJw 3.0 en iWmo 3.0)
