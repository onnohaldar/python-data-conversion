---
title: CS015-ref
description: 'CS015-ref: BerichtSubversie vullen met het het minornummer van de release van de iStandaard.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Elke versie van een iStandaard wordt aangeduid met een releasenummer, opgebouwd uit een major-, minor- en patchnummer in het format major.minor.patch (bijvoorbeeld: iWlz 2.3.0). Zie voor meer informatie het document Versiebeheer iStandaarden op https://istandaarden.nl/ibieb/versiebeheer-istandaarden.
