---
title: RS048-ref
description: 'RS048-ref: Vullen met een versienummer bestaande uit drie gehele getallen, gescheiden met punten.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
(NB: deze regel had regelcode CS128 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0 en iEb 1.0)
