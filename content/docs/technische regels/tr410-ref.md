---
title: TR410-ref
description: 'TR410-ref: Een Product mag alleen vaker in de berichtklassen NieuwProduct voorkomen als de zorgperiodes elkaar niet overlappen.'
wordtGebruiktIn: 'iWmo 3.1, iJw 3.1'
---
Indien de productcode niet is meegegeven dan geldt deze technische regel op niveau van productcategorie. Indien productcategorie leeg is, dan mag dit met geen enkel ander aangevraagd product overlappen. 

