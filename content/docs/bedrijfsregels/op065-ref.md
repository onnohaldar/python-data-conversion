---
title: OP065-ref
description: 'OP065-ref: Een retourbericht mag alleen informatie over clienten bevatten waarvan berichtklassen zijn afgekeurd.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
