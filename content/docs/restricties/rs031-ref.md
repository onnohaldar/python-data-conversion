---
title: RS031-ref
description: 'RS031-ref: Commentaarvelden niet leeg.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Ten behoeve van het uitsluiten van empty elements in het xml-bericht.

Bij optionele elementen van het type string is het in xml toegestaan om het element leeg op te nemen in een bericht. Doormiddel van het pattern is dit niet meer toegestaan en moet er altijd vulling zijn anders dan spaties.
