---
title: TR074-ref
description: 'TR074-ref: Indien StatusAanlevering de waarde '1' bevat, dan moet de sleutel van de betreffende aanlevering niet alleen uniek zijn binnen het bericht zelf, maar ook in combinatie met alle reeds ontvangen berichten.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
