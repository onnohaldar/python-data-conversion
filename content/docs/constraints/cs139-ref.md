---
title: CS139-ref
description: 'CS139-ref: Voor een Geboortedatum geldt dat DatumGebruik en Datum met elkaar in overeenstemming moeten zijn.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
(NB: deze regel had regelcode TR097 in de releases iWlz 2.2, iWmo 3.0 en iJw 3.0)

Concreet betekent dit dat:

Als DatumGebruik de waarde 3 heeft, moet Datum 01-01-1900 zijn;  
Als DatumGebruik de waarde 2 heeft, moet Datum 01-01-EEJJ zijn;  
Als DatumGebruik de waarde 1 heeft, moet Datum 01-MM-EEJJ zijn.
