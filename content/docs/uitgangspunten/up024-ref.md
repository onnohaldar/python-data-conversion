---
title: UP024-ref
description: 'UP024-ref: De informatieuitwisseling volgens de iStandaarden conformeert zich aan de wet- en regelgeving die voortvloeit uit de wet waarvoor de iStandaard het administratieve clientproces ondersteunt.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
