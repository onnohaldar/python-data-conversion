---
title: OP325-ref
description: 'OP325-ref: Een wijziging die met status aanlevering 2 wordt doorgegeven, overschrijft in zijn geheel de laatst verzonden aanlevering.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
Dit betekent dat het niet mogelijk is om voor een (1) client twee wijzigingen voor dezelfde aanlevering op dezelfde dag te versturen.
