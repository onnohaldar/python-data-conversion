---
title: OP192-ref
description: 'OP192-ref: Verzonden bestanden moeten voldoen aan de technische eisen.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
1. Het berichtuitwisselingsformaat is XML.
2. De bestandcodering is UTF-8.
3. Het gebruik van Byte-Order-Mark (BOM) is niet toegestaan.
4. Het einderegel teken is een combinatie van CR/LF (Windows einde-regel teken).
5. Gebruik xml als bestandextensie voor het bestand waar het XML bericht in opgenomen is.
