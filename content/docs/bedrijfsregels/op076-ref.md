---
title: OP076-ref
description: 'OP076-ref: Van iedere contactpersoon (relatie) moet worden opgegeven in welke relatie deze tot de client staat.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
