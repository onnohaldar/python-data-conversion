---
title: IV046-ref
description: 'IV046-ref: Welke gemeentecode moet gevuld worden?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iJw 3.0, iJw 3.1'
---
In de header van de berichten wordt de gemeente opgenomen die volgens de wet verantwoordelijk is voor zorg of ondersteuning aan de client. Voor aanduiding van de gemeente wordt de CBS codelijst gehanteerd.
