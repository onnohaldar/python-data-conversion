---
title: IV033-ref
description: 'IV033-ref: Hoe moet XsltVersie gevuld worden?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Wanneer de ontvanger fouten constateert in een bericht op basis van de ter beschikking gestelde XSLTs, wordt in het retourbericht aangegeven welke XSLT-versie gebruikt is voor de controle. Dit versienummer is opgenomen in de output van de XSLTs en dient overgenomen te worden in het retourbericht.

