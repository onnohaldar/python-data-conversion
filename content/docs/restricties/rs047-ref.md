---
title: RS047-ref
description: 'RS047-ref: Vullen met BerichtCode volgens de specificatie'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
(NB: deze regel had regelcode CS126 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0, iEb 1.0 en iPgb 2.0)
