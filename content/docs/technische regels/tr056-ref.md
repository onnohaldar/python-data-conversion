---
title: TR056-ref
description: 'TR056-ref: Identificatie moet per berichtsoort uniek zijn voor de verzendende partij.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
