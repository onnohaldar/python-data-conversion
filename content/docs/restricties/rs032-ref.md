---
title: RS032-ref
description: 'RS032-ref: Datum vullen zonder tijdzone.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Bij datatypen van het type date is het niet toegestaan een Tijdszone mee te geven. 
Bijvoorbeeld: 
2016-08-30 is goed
2016-08-30Z is niet goed
