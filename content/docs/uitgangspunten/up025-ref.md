---
title: UP025-ref
description: 'UP025-ref: De geleverde zorg of ondersteuning wordt gedeclareerd op clientniveau.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
