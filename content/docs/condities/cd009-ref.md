---
title: CD009-ref
description: 'CD009-ref: Alleen als Soort adres de waarde 04 (tijdelijk adres) heeft, is vullen toegestaan.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
