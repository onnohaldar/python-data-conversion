# packageimports
from openpyxl import load_workbook
import os
from humps import camelize

# Class to Store iStandaard Regel
class IstdRegel:
    def __init__(self):
        self.type: str = None
        self.code: str = None
        self.name: str = None
        self.documentatie = None
        self.wordtGebruiktIn = None

# build HUGO Markdown content / docs path for regelType
def docsRegelPath(regelType: str):
    return os.path.join(os.getcwd(), 'content', 'docs', regelType.lower())

# return string between single quotes
def betweenSingleQuotes(inputStr: str):
    if inputStr != None:
        return "'" + inputStr + "'"
    else:
        return "''"

# write HUGO Markdown content Index File for regeType
def writeIndexFile(regelType: str):
    regelPath = docsRegelPath(regelType)
    os.makedirs(regelPath, exist_ok=True)
    indexFilePath = os.path.join(regelPath, '_index.md')

    with open(indexFilePath, 'w') as indexFile:
        print('---', file=indexFile)
        print('title: ' + regelType, file=indexFile)
        print('---', file=indexFile)

# write HUGO Markdown content Regel Files for regeType
def writeRegelsToHugoMdContentFiles(regelType: str, regels = []):
    for regel in regels:
        # write regel file
        regelPath = docsRegelPath(regelType)
        regelFile = regel.code.lower() + '.md'
        regelFilePath = os.path.join(regelPath, regelFile)
        with open(regelFilePath, 'w') as regelFile:
            print('---', file=regelFile)
            print('title: ' + regel.code, file=regelFile)
            print('description: ' + betweenSingleQuotes(regel.name), file=regelFile)
            print('wordtGebruiktIn: ' + betweenSingleQuotes(regel.wordtGebruiktIn), file=regelFile)
            print('---', file=regelFile)
            if regel.documentatie != None:
                print(regel.documentatie, file=regelFile)


# convert BizDesign export Excel File with regels to HUGO Markdown Content Files
def convertBizXlsToHugoContent(regelExportFilePath: str):
    wb = load_workbook(filename=regelExportFilePath)
    ws = wb['Blad1']
    columnNames = []
    regels = []
    rowNr: int = 0
    # regeltype staat altijd in cell B2
    regelType: str = ws['B2'].value
    writeIndexFile(regelType)

    # process every row in the worksheet
    for wsRow in ws:
        if rowNr == 0:
            # add column names from the first row
            for wsCell in wsRow:
                columnNames.append(camelize(wsCell.value))
        elif rowNr > 1:
            # every row from third row contains data values
            columnNr = 0
            # create new iStandaard regel instance
            regel = IstdRegel()
            # process cell value for every column in the row
            for wsCell in wsRow:
                # map column cell values to regel instance
                columnName: str = columnNames[columnNr]
                if columnName == 'type':
                    regel.type = wsCell.value
                elif columnName == 'code':
                    regel.code = wsCell.value
                elif columnName == 'name':
                    regel.name = wsCell.value
                elif columnName == 'documentatie':
                    regel.documentatie = wsCell.value
                elif columnName == 'wordtGebruiktIn':
                    regel.wordtGebruiktIn = wsCell.value
                # next column to process
                columnNr = columnNr + 1
            # add processed regel instance
            regels.append(regel)
        # process next row
        rowNr = rowNr + 1

    writeRegelsToHugoMdContentFiles(regelType, regels)

# Main Conversion Script
inputPath = 'input-data/'

# process xlsx files
for inputFile in os.listdir(inputPath):
    if inputFile.endswith('.xlsx'):
        regelExportFilePath = os.path.join(inputPath, inputFile)
        print(regelExportFilePath)
        convertBizXlsToHugoContent(regelExportFilePath)
