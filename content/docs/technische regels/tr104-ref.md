---
title: TR104-ref
description: 'TR104-ref: Een verwijderd bericht kan niet gewijzigd worden.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
Een bericht dat eerder met status aanlevering '3' (Verwijderen aanlevering) verwijderd is, kan niet gewijzigd worden met status aanlevering '2' (Gewijzigde aanlevering). 
