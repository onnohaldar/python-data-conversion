---
title: TR378-ref
description: 'TR378-ref: Vullen met een bestaande gemeentecode uit het overzicht van CBS.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iPgb 2.0, iJw 3.0'
---
(NB: deze regel had regelcode CS300 in de releases iJw 3.0, iWmo 3.0, iEb 1.0 en iPgb 2.0)

De gemeentecode moet voorkomen in de Gemeentelijke indeling (zie https://www.cbs.nl)
