---
title: UP016-ref
description: 'UP016-ref: Per iStandaard worden verschillende soorten zorg of ondersteuning  uitgedrukt volgens een vastgestelde codelijst.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
