---
title: OP035-ref
description: 'OP035-ref: Mutaties in de zorglevering worden alleen doorgegeven aan het CAK wanneer deze van invloed kunnen zijn op de eigen bijdrage.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
