---
title: OP047-ref
description: 'OP047-ref: Als een bericht niet aan de geldende standaard voldoet, mag het bericht afgekeurd worden.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
