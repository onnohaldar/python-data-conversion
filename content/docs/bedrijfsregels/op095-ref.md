---
title: OP095-ref
description: 'OP095-ref: Een bericht mag niet worden afgekeurd op basis van informatie waartoe de verzendende partij geen toegang heeft.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
