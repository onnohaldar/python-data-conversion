---
title: UP007-ref
description: 'UP007-ref: Het recht van de client op de inzet van zorg of ondersteuning wordt door de instantie die verantwoordelijk is voor de inzet van zorg of ondersteuning voor de client vastgelegd in een besluit of beschikking.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
