---
title: OP186-ref
description: 'OP186-ref: Het beeindigen van een toewijzing op een datum die in het verleden ligt kan alleen na overleg met de betreffende aanbieder.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Deze regel is van toepassing op die situaties waarin de aanbieder niet had kunnen weten dat hij geen zorg of ondersteuning meer mocht leveren.
