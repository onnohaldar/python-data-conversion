---
title: UP006-ref
description: 'UP006-ref: De client wordt in de iStandaarden geidentificeerd met zijn BSN.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
