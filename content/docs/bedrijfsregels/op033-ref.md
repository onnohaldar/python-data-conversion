---
title: OP033-ref
description: 'OP033-ref: Voor het doorgeven van wijzigingen en correcties op een eerder verzonden bericht, moet gebruik gemaakt worden van de systematiek van status aanlevering.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Met de status aanlevering van een berichtklasse kan worden aangegeven of: 
* een berichtklasse nieuw is (waarde 1)
* een berichtklasse gewijzigd is (waarde 2)
* een berichtklasse verwijderd moet worden (waarde 3). Een verwijdering betekent dat de vorige aanlevering met dezelfde sleutel als niet verzonden beschouwd moet worden. 
