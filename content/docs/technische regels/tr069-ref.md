---
title: TR069-ref
description: 'TR069-ref: Gegevens in een melding van de mutatie of beeindiging van een zorglevering die betrekking hebben op de start van diezelfde levering moeten overeenkomen met de gegevens die bij de melding van de start van die levering zijn aangeleverd.'
wordtGebruiktIn: 'iWmo 3.0, iWlz 2.3, iJw 3.0, iWlz 2.2'
---
