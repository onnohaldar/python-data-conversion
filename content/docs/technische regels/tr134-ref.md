---
title: TR134-ref
description: 'TR134-ref: Vullen met een bestaande datum die niet groter is dan de Dagtekening van het bericht.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iWlz 2.2'
---
(NB: deze regel had regelcode CS023 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0 en iPgb 2.0)
