---
title: TR002-ref
description: 'TR002-ref: Geboortedatum mag niet meer dan 120 jaar voor de Dagtekening liggen, tenzij de geboortedatum onbekend is.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Geboortedatum is onbekend als DatumGebruik de waarde 3 (dag, maand en jaar onbekend) heeft en Datum de waarde 01-01-1900 heeft.

