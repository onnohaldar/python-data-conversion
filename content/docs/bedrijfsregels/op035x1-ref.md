---
title: OP035x1-ref
description: 'OP035x1-ref: Per iStandaard worden afspraken gemaakt of het overlijden van een client wel of niet als mutatie doorgestuurd wordt aan het CAK.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
Deze informatie ontvangt het CAK van het BRP en hoeft niet of mag niet doorgestuurd worden aan het CAK. 
