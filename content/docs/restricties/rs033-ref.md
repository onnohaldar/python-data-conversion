---
title: RS033-ref
description: 'RS033-ref: Geen lege elementen in XML.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Ten behoeve van het uitsluiten van empty elements in het xml-bericht.

Bij optionele elementen van het type string is het in xml toegestaan om het element leeg op te nemen in een bericht. Doormiddel van het pattern is dit niet meer toegestaan en moet er altijd vulling zijn anders dan spaties.
