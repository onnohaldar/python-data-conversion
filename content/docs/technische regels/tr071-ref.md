---
title: TR071-ref
description: 'TR071-ref: StatusAanlevering mag niet de waarde '3' bevatten als er voor de betreffende melding van de start van de zorglevering al gemeld is dat diezelfde levering beeindigd is.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
De melding van de start van een zorglevering mag niet verwijderd worden wanneer voor diezelfde zorglevering al een beeindiging gemeld is.
