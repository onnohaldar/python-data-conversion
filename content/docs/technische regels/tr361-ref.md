---
title: TR361-ref
description: 'TR361-ref: Een Product mag alleen vaker in verzoek om wijziging voorkomen als de zorgperiodes elkaar niet overlappen'
wordtGebruiktIn: 'iWmo 3.1, iJw 3.1'
---
Voor producten die zijn opgenomen in de berichtklasse TeWijzigenProduct of OngewijzigdProduct moet op basis van het ToewijzingNummer de overlap op de aangevraagde producten gecontroleerd worden. Indien de productcode niet is meegegeven dan geldt deze technische regel op niveau van productcategorie. Indien productcategorie leeg is, dan mag dit met geen enkel ander aangevraagd product overlappen. 
