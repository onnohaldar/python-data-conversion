---
title: TR061-ref
description: 'TR061-ref: Bij een client moet een adres voorkomen dat gebruikt kan worden voor correspondentie met de client.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
