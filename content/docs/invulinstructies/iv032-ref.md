---
title: IV032-ref
description: 'IV032-ref: Welke retourcode moet gevuld worden in het retourbericht?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Welke retourcode gevuld moet worden, wordt bepaald door de controle op basis waarvan het bericht wordt afgekeurd. Deze controles zijn beschreven als technisch te controleren regels die op verschillende niveaus gecontroleerd worden. Bij iedere technisch te controleren regel is aangegeven op welk controleniveau deze gecontroleerd wordt. Alle technisch te controleren regels vanaf controleniveau 2 kennen een retourcode. Deze retourcode wordt in het retourbericht gebruikt wanneer het heenbericht op basis van deze regel wordt afgekeurd. 

Een ontvangen heenbericht wordt op vier niveaus gecontroleerd: 
Controleniveau 1: berichtformaat (xsd)
Het bericht wordt gevalideerd tegen het XSD. 

Indien het bestand niet valideert, krijgt de afzender een foutmelding. Er wordt geen retourbericht verzonden. Regels op dit controleniveau hebben daarom geen eigen retourcode.

Controleniveau 2: berichtinhoud (xslt)
Het bericht wordt gecontroleerd tegen alle regels (technische regels, condities en constraints) die binnen het bericht zelf te controleren zijn. Voor deze regels zijn XSLTs beschikbaar die gebruikt kunnen worden om de controles uit te voeren. 

Welke retourcode voor deze regels gebruikt moet of mag worden, is per iStandaard vastgelegd. 

Controleniveau 3: berichtoverstijgend
Het bericht wordt gecontroleerd op alle technische regels die berichtoverstijgend zijn. Dat wil zeggen dat de informatie in het heenbericht gecontroleerd wordt ten opzichte van informatie in één of meer eerder ontvangen berichten. 

Deze regels hebben een eigen retourcode die gevuld wordt in het retourbericht bij de berichtklasse waarin de fout geconstateerd is.

Controleniveau 4: externe bron
Het bericht wordt gecontroleerd op alle technische regels waarvoor informatie nodig is die geen onderdeel is van het berichtenverkeer. Dit betreft bijvoorbeeld: 
* Systeemdatum van de verzender/ontvanger;
* Een externe codelijst zoals de iWlz-AGB-codelijst of de CBS gemeentecodes;
* Bilaterale afspraken tussen ketenpartijen, zoals contractafspraken.

Deze regels hebben een eigen retourcode die gevuld wordt in het retourbericht bij de berichtklasse waarin de fout geconstateerd is.
