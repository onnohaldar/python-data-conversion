---
title: CS089-ref
description: 'CS089-ref: Als LandCode de waarde NL (Nederland) heeft, dan moet het formaat overeenkomen met dat van een Nederlandse postcode.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Concreet betekent dit dat de waarde moet voldoen aan de reguliere expressie: [1-9][0-9]{3}[a-zA-Z]{2}. 
Zie ook https://www.postcode.nl/.
