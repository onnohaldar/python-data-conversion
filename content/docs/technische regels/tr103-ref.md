---
title: TR103-ref
description: 'TR103-ref: Indien StatusAanlevering de waarde '2' (Gewijzigde aanlevering) bevat, dan moet voor de betreffende client een eerdere aanlevering met StatusAanlevering '1' (Eerste aanlevering) verstuurd zijn met dezelfde sleutel.'
wordtGebruiktIn: 'iEb 1.0, iWlz 2.3, iWlz 2.2'
---
Een inzending met status aanlevering '2' (Gewijzigde aanlevering) kan alleen indien eerder voor dezelfde Client een bericht met Status aanlevering '1' (Eerste aanlevering) is ingezonden met dezelfde sleutel.  

De meest recente aanlevering, met status aanlevering '2' (Gewijzigde aanlevering), vervangt de eerder verzonden aanlevering met status aanlevering '1' of '2'. Het is daarom noodzakelijk om bij gebruik van status aanlevering 2 het retourbericht af te wachten voordat een eventueel volgende correctie met status 2 of 3 wordt ingestuurd.
