---
title: OP087-ref
description: 'OP087-ref: In een toewijzingbericht moet een compleet beeld van alle actuele toewijzingen worden opgenomen.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Een actuele toewijzing is een toewijzing die op of na de aanmaakdatum van het bericht geldig is.
