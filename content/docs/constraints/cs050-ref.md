---
title: CS050-ref
description: 'CS050-ref: Als Partnernaam gevuld is, dan NaamGebruik vullen met waarde 1, 2, 3 of 4. Anders waarde 1 vullen.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Als Partnernaam gevuld is, wordt in NaamGebruik aangegeven hoe de persoon zijn naam wenst te gebruiken. Hiervoor kunnen de volgende waarden gebruikt worden: 1 (eigen naam), 2 (naam echtgenoot of geregistreerd partner of alternatieve naam), 3 (naam echtgenoot of geregistreerd partner gevolgd door eigen naam) of 4 (eigen naam gevolgd door naam echtgenoot of geregistreerd partner). Indien geen Partnernaam gevuld is, wordt verplicht 1 (eigen naam) gevuld.
