---
title: TR302-ref
description: 'TR302-ref: Een Product mag alleen vaker in een toewijzingsbericht voorkomen indien de zorgperiodes elkaar niet overlappen'
wordtGebruiktIn: 'iWmo 3.1, iJw 3.1'
---
Indien de productcode niet is meegegeven dan geldt deze technische regel op niveau van productcategorie. Indien productcategorie leeg is, dan mag de toewijzing met geen enkel ander toegewezen product overlappen.  

Verwijderde toewijzingen (waarvan de einddatum gelijk is aan de ingangsdatum en de reden wijziging is gevuld met 13 (Verwijderd)) maken geen onderdeel uit van de bepaling of er sprake is van stapeling. Deze toewijzingen worden niet gezien als actuele toewijzing.  Hetzelfde geldt voor toewijzingen met reden wijziging 01 (Administratieve correctie (vervallen)) en einddatum gelijk aan ingangsdatum.
