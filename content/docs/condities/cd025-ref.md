---
title: CD025-ref
description: 'CD025-ref: Als Adres / LandCode de waarde NL (Nederland) heeft, dan verplicht vullen.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
