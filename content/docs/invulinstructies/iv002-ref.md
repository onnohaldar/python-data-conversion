---
title: IV002-ref
description: 'IV002-ref: Hoe om te gaan met de adressering indien er sprake is van een organisatie?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Alleen vullen indien client en/of relatie client in instelling verblijft.
