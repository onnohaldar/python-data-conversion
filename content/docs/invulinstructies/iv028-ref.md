---
title: IV028-ref
description: 'IV028-ref: Hoe wordt een retourbericht opgesteld?'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Een retourbericht wordt gestuurd om de zender te informeren over de beoordeling (technisch/inhoudelijk) van het heenbericht. 

Een retourbericht wordt als volgt opgebouwd: 
Een retourbericht bevat altijd een Header: 
* In de header wordt de BerichtCode gevuld met de berichtcode van het betreffende retourbericht. 
* Overige elementen, inclusief Afzender en Ontvanger, worden ongewijzigd overgenomen van de header van het heenbericht. 
* De header wordt aangevuld met identificerende gegevens van het retourbericht (IdentificatieRetour en DagtekeningRetour) en kan worden aangevuld met het versienummer van de XSLTs die gebruikt zijn om het bericht te controleren.

Wat na de header volgt is afhankelijk van of en op welk controleniveau fouten zijn geconstateerd:

Er zijn geen fouten geconstateerd:
Wanneer geen fouten geconstateerd zijn is het heenbericht volledig goedgekeurd. Het retourbericht bevat in dat geval alleen een Header, zonder retourcodes.

Controleniveau 1: Er zijn fouten geconstateerd bij XSD validatie 
Indien het bestand niet valideert tegen het XSD krijgt de afzender een foutmelding. Er wordt geen retourbericht verzonden.

Controleniveau 2: Er zijn fouten geconstateerd bij XSLT validatie
Voor alle regels die binnen één bericht gecontroleerd kunnen worden, maar die niet via het XSD gevalideerd kunnen worden, zijn XSLTs beschikbaar die gebruikt kunnen worden om de controles uit te voeren. Wanneer een bericht een fout oplevert bij een controle op één van deze regels wordt alleen de Header teruggestuurd met de algemene retourcode 0001 (Bericht is afgekeurd om technische redenen.). Het versienummer van de XSLTs die gebruikt zijn bij de controles moet verplicht worden ingevuld. 

Controleniveau 3 of 4: Er zijn fouten geconstateerd op berichtoverstijgende controles of controles tegen een externe bron
Indien er een fout geconstateerd is in de Header, bevat het retourbericht alleen de Header met daarbij de retourcode van de regel op basis waarvan de fout geconstateerd is.
Indien in één of meer berichtklassen over een Client fouten zijn geconstateerd, worden alle berichtklassen van die Client ongewijzigd overgenomen in het retourbericht. Bij iedere berichtklasse wordt telkens met een retourcode aangegeven wat de status is:
* Geen fouten geconstateerd in deze berichtklasse: retourcode 0200
* Deze berichtklasse is niet gecontroleerd: retourcode 0233 
* Er is een fout geconstateerd in deze berichtklasse: de retourcode van de regel op basis waarvan de fout geconstateerd is.

NB: Een bericht kan informatie over meerdere clienten bevatten. Clienten waarin geen fouten zijn geconstateerd, worden niet opgenomen in het retourbericht.
