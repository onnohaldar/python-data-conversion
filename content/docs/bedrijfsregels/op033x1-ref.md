---
title: OP033x1-ref
description: 'OP033x1-ref: Een wijziging van een afgegeven toewijzing moet met een nieuw bericht worden doorgegeven.'
wordtGebruiktIn: 'iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2'
---
Het wijzigen van een afgegeven toewijzing is maar beperkt toegestaan.
